import React from "react";
import { Redirect } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Switch from "@mui/material/Switch";
import NavBar from "../component/NavBar";
import axios from "axios";

class Login extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      checkedA: true
    };
  }
  handleChange = e => {
    console.log(e.target.value);
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSwitchChange = e => {
    console.log(e.target.value);
    this.setState({ [e.target.name]: e.target.checked });
  };
  submitUser = () => {
    let payload = {
      email: this.state.email,
      password: this.state.password,
      sso: this.state.checkedA
    };

    let userQuery = `http://${process.env.REACT_APP_SERVER_ADDRESS}/api/users/login`;
    axios
      .post(userQuery, payload)
      .then(d => {
        this.setState({ email: "" });
        this.setState({ password: "" });
        this.setState({ checkedA: false });

        if (d.data.token) {
          localStorage.setItem("jwt", d.data.token);
          this.props.history.push("/dashboard");
        }
      })
      .catch(e => {
        //this.message = e;
      });
  };
  render() {
    return (
      <>
        <CssBaseline />

        <NavBar />
        <Container maxWidth="lg">
          <Box
            component="main"
            sx={{ flexGrow: 1, p: 3, marginTop: "3em", marginLeft: "300px" }}
          >
            <Grid container spacing={2}>
              <Grid item xs={8}>
                <Box component="span" sx={{ textAlign: "center" }}>
                  <h1>Login</h1>
                </Box>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" },
                    textAlign: "center"
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-basic"
                    label="email"
                    variant="outlined"
                    name="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                  />

                  <TextField
                    id="outlined-password-input"
                    label="password"
                    variant="outlined"
                    name="password"
                    type="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                  />
                  <h4>use SSO Authentication</h4>
                  <Box>
                    <Switch
                      checked={this.state.checkedA}
                      onChange={this.handleSwitchChange}
                      color="primary"
                      name="checkedA"
                      inputProps={{ "aria-label": "primary checkbox" }}
                    />
                  </Box>
                  <br></br>
                  <Button
                    sx={{ color: "white", backgroundColor: "black" }}
                    variant="contained"
                    onClick={this.submitUser}
                  >
                    Submit
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </>
    );
  }
}

export default Login;
