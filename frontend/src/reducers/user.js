const user = (
  state = {
    selectedUsers: []
  },
  action
) => {
  switch (action.type) {
    case "SET_SELECTED_USERS":
      return { ...state, selectedUsers: action.name };
    default:
      return state;
  }
};
export default user;
