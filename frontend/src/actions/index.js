export const setSelectedUsers = name => {
  return {
    type: "SET_SELECTED_USERS",
    name
  };
};
