import React from "react";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import axios from "axios";

class AddUser extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      confirmpassword: "",
      role: "user"
    };
  }
  handleChange = e => {
    console.log(e.target.value);
    this.setState({ [e.target.name]: e.target.value });
  };
  submitUser = () => {
    let payload;
    if (this.state.password === this.state.confirmpassword) {
      payload = {
        email: this.state.email,
        password: this.state.password,
        role: this.state.role
      };
    }
    let userQuery = `http://${process.env.REACT_APP_SERVER_ADDRESS}/api/users/register`;
    axios
      .post(userQuery, payload)
      .then(d => {
        this.setState({ email: "" });
        this.setState({ password: "" });
        this.setState({ confirmpassword: "" });
        this.setState({ role: "user" });
        if (d.data.token) {
          localStorage.setItem("jwt", d.data.token);
        }
      })
      .catch(e => {
        //this.message = e;
      });
  };
  render() {
    return (
      <>
        <Container maxWidth="lg">
          <Box component="main" sx={{ flexGrow: 1, p: 3, marginTop: "3em" }}>
            <Grid container spacing={2}>
              <h4>add user</h4>
              <Grid item xs={8}>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-basic"
                    label="email"
                    variant="outlined"
                    name="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                  />
                </Box>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <FormControl component="fieldset">
                    <FormLabel component="legend"></FormLabel>
                    <RadioGroup
                      row
                      aria-label="role"
                      name="role"
                      onChange={this.handleChange}
                    >
                      <FormControlLabel
                        value="client"
                        control={<Radio />}
                        label="Client"
                      />
                      <FormControlLabel
                        value="user"
                        control={<Radio />}
                        label="User"
                      />
                      <FormControlLabel
                        value="admin"
                        control={<Radio />}
                        label="Admin"
                      />
                    </RadioGroup>
                  </FormControl>
                </Box>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-password-input"
                    label="password"
                    variant="outlined"
                    name="password"
                    type="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                  />
                  <br></br>
                  <TextField
                    id="outlined-password-input"
                    label="confirm password"
                    variant="outlined"
                    name="confirmpassword"
                    type="password"
                    value={this.state.confirmpassword}
                    onChange={this.handleChange}
                  />
                  <br></br>
                  <Button variant="contained" onClick={this.submitUser}>
                    Submit
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </>
    );
  }
}

export default AddUser;
