import React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Checkbox from "@mui/material/Checkbox";
import { connect } from "react-redux";
import axios from "axios";
import { setSelectedUsers } from "../actions";
// use a test list from here
// as a user from list is checked, it is added to store delete list

class Userlist extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      userList: [],
      selectedUsers: []
    };
    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    let userQuery = `http://${process.env.REACT_APP_SERVER_ADDRESS}/api/users`;
    axios
      .get(userQuery)
      .then(d => {
        this.setState({ userList: d.data });
        console.log(d);
      })
      .catch(e => {
        console.log(e);
      });
  }
  handleChange(event) {
    //
    let newList = [];
    let oldList = this.state.selectedUsers;
    if (event.target.checked === true) {
      oldList.push(event.target.value);
      newList = oldList;
    } else if (event.target.checked === false) {
      let newList = oldList.filter(id => {
        return id !== event.target.value;
      });
    }
    this.setState({ selectedUsers: newList });
    this.props.setSelectedUsers(newList);
    console.log(this.state.selectedUsers);
    console.log(event.target.checked);
  }
  render() {
    let showUserList = this.state.userList.map((item, index) => {
      return (
        <ListItem key={index} value={item.id}>
          <ListItemButton role={undefined} dense>
            <ListItemIcon>
              <Checkbox
                edge="start"
                onChange={this.handleChange}
                tabIndex={-1}
                disableRipple
                value={item.id}
              />
            </ListItemIcon>
            <ListItemText id={item.id} primary={item.email} />
          </ListItemButton>
        </ListItem>
      );
    }, this);
    return (
      <>
        <List>{showUserList}</List>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedUsers: state.selectedUsers
  };
};
const mapDispatchToProps = () => {
  return {
    setSelectedUsers
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(Userlist);
