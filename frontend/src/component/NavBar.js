import React from "react";
import { Redirect } from "react-router-dom";
import Link from "@mui/material/Link";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";

class NavBar extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      navigate: false
    };
  }
  render() {
    const { navigate } = this.state;
    if (navigate) {
      localStorage.setItem("jwt", "");
      return <Redirect to="/" push={true} />;
    }
    return (
      <>
        <CssBaseline />
        <AppBar
          position="absolute"
          sx={{
            zIndex: theme => theme.zIndex.drawer + 10,
            backgroundColor: "black",
            color: "white",
            borderBottom: "1px solid white"
          }}
        >
          <Toolbar>
            <Container sx={{ textAlign: "left" }}>
              <Typography variant="h2" color="inherit">
                <Link sx={{ color: "white" }} href="/" underline="none">
                  Radius Demo
                </Link>
              </Typography>
            </Container>
            <Container sx={{ textAlign: "right" }}>
              <Link
                sx={{ color: "white", fontSize: "20px", marginLeft: "1em" }}
                underline="none"
                href="/dashboard"
              >
                dashboard
              </Link>
              <Link
                sx={{ color: "white", fontSize: "20px", marginLeft: "1em" }}
                underline="none"
                href="/signup"
              >
                sign up
              </Link>
              <Link
                sx={{ color: "white", fontSize: "20px", marginLeft: "1em" }}
                underline="none"
                href="/login"
              >
                login
              </Link>
              <Link
                sx={{ color: "white", fontSize: "20px", marginLeft: "1em" }}
                underline="none"
                onClick={() => this.setState({ navigate: true })}
              >
                logout
              </Link>
            </Container>
          </Toolbar>
        </AppBar>
      </>
    );
  }
}

export default NavBar;
