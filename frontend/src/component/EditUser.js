import React from "react";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { connect } from "react-redux";
import axios from "axios";

class EditUser extends React.PureComponent {
  // /api/users/update/id?tochange=thing
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      role: ""
    };
  }
  handleChange = e => {
    console.log(e.target.value);
    this.setState({ [e.target.name]: e.target.value });
  };
  submitUser = () => {
    // if input has something in it, use it to make query
    // get selected user to edit from global store, must be only one selected
    let userId;
    let query = "";
    if (this.props.selectedUsers.length === 1) {
      userId = this.props.selectedUsers[0];
    }
    let counter = 0;
    ["email", "password", "role"].forEach(item => {
      if (this.state[item] && counter === 0) {
        query += `${item}=${this.state[item]}`;
        counter++;
      } else if (this.state[item] && counter !== 0) {
        query += `&${item}=${this.state[item]}`;
        counter++;
      }
    });
    let uri = `http://${process.env.REACT_APP_SERVER_ADDRESS}/api/users/update/${userId}?${query}`;

    axios
      .put(uri)
      .then(d => {
        this.setState({ email: "" });
        this.setState({ password: "" });
        this.setState({ role: "" });
      })
      .catch(e => {
        //this.message = e;
      });
  };
  render() {
    return (
      <>
        <Container maxWidth="lg">
          <Box component="main" sx={{ flexGrow: 1, p: 3, marginTop: "3em" }}>
            <Grid container spacing={2}>
              <h4>edit user</h4>
              <Grid item xs={8}>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-basic"
                    label="email"
                    variant="outlined"
                    name="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                  />
                </Box>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <FormControl component="fieldset">
                    <FormLabel component="legend"></FormLabel>
                    <RadioGroup
                      row
                      aria-label="role"
                      name="role"
                      onChange={this.handleChange}
                    >
                      <FormControlLabel
                        value="client"
                        control={<Radio />}
                        label="Client"
                      />
                      <FormControlLabel
                        value="user"
                        control={<Radio />}
                        label="User"
                      />
                      <FormControlLabel
                        value="admin"
                        control={<Radio />}
                        label="Admin"
                      />
                    </RadioGroup>
                  </FormControl>
                </Box>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-password-input"
                    label="password"
                    variant="outlined"
                    name="password"
                    type="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                  />
                  <br></br>

                  <Button variant="contained" onClick={this.submitUser}>
                    Submit
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedUsers: state.selectedUsers
  };
};
export default connect(mapStateToProps)(EditUser);
