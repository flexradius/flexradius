import React from "react";
import { connect } from "react-redux";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import axios from "axios";

class DeleteUser extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      id: null
    };
  }
  submitUser = () => {
    // selected User is array so make it like id=one&id=two&id=three
    let query = "";
    if (this.props.selectedUsers.length > 0) {
      for (let i = 0; i < this.props.selectedUsers.length; i++) {
        if (i === 0) {
          query += `id=${this.props.selectedUsers[i]}`;
        } else {
          query += `&id=${this.props.selectedUsers[i]}`;
        }
      }
    }
    let userQuery = `http://${process.env.REACT_APP_SERVER_ADDRESS}/api/users/delete?${query}`;
    axios
      .delete(userQuery)
      .then(d => {
        this.setState({ email: "" });

        if (d.data.token) {
          localStorage.setItem("jwt", d.data.token);
        }
      })
      .catch(e => {
        //this.message = e;
      });
  };
  render() {
    return (
      <>
        <Container maxWidth="lg">
          <Box component="main" sx={{ flexGrow: 1, p: 3, marginTop: "3em" }}>
            <Grid container spacing={2}>
              <h4>delete user</h4>
              {this.props.selectedUsers}
              <Grid item xs={8}>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <br></br>
                  <Button variant="contained" onClick={this.submitUser}>
                    Delete Selected Users
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    selectedUsers: state.selectedUsers
  };
};
export default connect(mapStateToProps)(DeleteUser);
