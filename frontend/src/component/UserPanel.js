import React from "react";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import AddUser from "./AddUser";
import EditUser from "./EditUser";
import DeleteUser from "./DeleteUser";
// use a test list from here

class UserPanel extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selection: 1
    };
  }

  render() {
    return (
      <>
        <Stack direction="row" spacing={2}>
          <Button
            onClick={() => this.setState({ selection: 1 })}
            variant="outlined"
          >
            Add User
          </Button>
          <Button
            onClick={() => this.setState({ selection: 2 })}
            variant="outlined"
          >
            Edit User
          </Button>
          <Button
            onClick={() => this.setState({ selection: 3 })}
            variant="outlined"
          >
            Delete User
          </Button>
        </Stack>
        {this.state.selection === 1 && <AddUser />}
        {this.state.selection === 2 && <EditUser />}
        {this.state.selection === 3 && <DeleteUser />}
      </>
    );
  }
}

export default UserPanel;
