CREATE DATABASE users;
\c users;
CREATE TABLE users (id SERIAL PRIMARY KEY, email varchar(255), role varchar(255), password varchar(255));
INSERT INTO users(email, role, password) VALUES('bob@gmail.com', 'user', '$2b$10$pijIJT38X4zGmCise0n3P..EEJdxkLb8/gEQ/g0oVD5TIU8F.KGku');
