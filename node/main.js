const express = require("express");
const cors = require("cors");
const app = express();
const radiusAccountingServer = require("./radius/radiusServer.js");
const radiusAuthenticationServer = require("./radius/radiusServer.js");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const path = require("path");
const session = require("express-session");
const dotenv = require("dotenv").config();
const port = 8000;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/client/dist"));
app.use(express.json());
app.use(cookieParser());
app.use(
  session({
    secret: "hideme",
    cookie: { maxAge: 60000 },
    resave: true,
    saveUninitialized: true
  })
);

require("./config/routes.js")(app);

app.use(function(req, res, next) {
  res.status(404).send("Sorry can't find that!");
});

app.listen(port, () => {
  console.log(`thefudlist app listening at http://localhost:${port}`);
});
// 1800 will be radius auth and 1801 will be
// listender for radius accounting
//radiusAccountingServer.bind(1801);
// auth server
//radiusAuthenticationServer.bind(1800);
