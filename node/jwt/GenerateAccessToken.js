const jwt = require("jsonwebtoken");

class GenerateAccessToken {
  jwt(username) {
    return jwt.sign(
      { exp: Math.floor(Date.now() / 1000) + 60, data: username },
      process.env.TOKEN_SECRET
    );
  }
}
module.exports = new GenerateAccessToken();
