const pool = require("../config/postgresConfig");
const bcrypt = require("bcrypt");
const GenerateAccessToken = require("../jwt/GenerateAccessToken");

class User {
  async read(email) {
    let newQuery = `SELECT * FROM users WHERE email='${email}'`;
    let dbres = await pool.query(newQuery);
    if (dbres.rows.length > 0) {
      return dbres.rows[0];
    }
  }
  async create(user) {
    // need to add dup check
    const salt = bcrypt.genSaltSync();
    const passHash = bcrypt.hashSync(user.password, salt);
    let newQuery = `INSERT INTO users (email, role, password) VALUES ('${user.email}','${user.role}','${passHash}') RETURNING id`;
    let dbres = await pool.query(newQuery);
    console.log(dbres);
    if (dbres.rows.length > 0) {
      return GenerateAccessToken.jwt(user.email);
    }
  }
  async readAll() {
    console.log("read all method fires for readAll");
    let newQuery = "SELECT * FROM users";
    let dbres = await pool.query(newQuery);
    if (dbres.rows.length > 0) {
      return dbres.rows;
    }
  }
  async update(id, query) {
    // update <table> set column=value, column=value where condition;
    // gen string for sets, insert into greater query string
    let innerQuery = "";
    let counter = 0;
    ["email", "password", "role"].forEach(item => {
      if (counter === 0) {
        innerQuery += `${item}='${query[item]}'`;
        counter++;
      } else {
        innerQuery += `,${item}='${query[item]}'`;
        counter++;
      }
    });
    console.log(innerQuery);
    let newQuery = `UPDATE users SET ${innerQuery} WHERE id=${id}`;
    let dbres = await pool.query(newQuery);
    if (dbres.rows.length > 0) {
      return dbres.rows;
    }
  }
  async delete(id) {
    let newQuery = `DELETE FROM users WHERE id=${id}`;
    let dbres = await pool.query(newQuery);
    if (dbres.rows.length > 0) {
      return dbres.rows;
    }
  }
  async authorize(user) {
    const newQuery = `SELECT password FROM users WHERE email='${user.email}'`;
    let dbres = await pool.query(newQuery);
    if (dbres.rows.length > 0) {
      let dbuser = dbres.rows[0];
      const validPassword = bcrypt.compareSync(user.password, dbuser.password);
      if (validPassword) {
        return GenerateAccessToken.jwt(user.email);
      }
    }
  }
}

module.exports = new User();
