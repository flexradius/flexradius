let UserController = require("../controllers/UserController");

module.exports = app => {
  app.get("/api/users", UserController.all);
  app.get("/api/users/:id", UserController.read);
  app.post("/api/users/register", UserController.register);
  app.post("/api/users/login", UserController.login);
  app.delete("/api/users/delete", UserController.delete);
  app.put("/api/users/update/:id", UserController.update);
  app.post("/api/users/authenticate-token", UserController.authenticateToken);
  app.post("/api/users/validate-token", UserController.validateToken);
};
