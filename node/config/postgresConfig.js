const { Pool, Client } = require("pg");

const postgres = new Pool({
  user: "postgres",
  host: "localhost",
  password: "postgres",
  database: "users",
  port: "5432"
});

module.exports = postgres;
