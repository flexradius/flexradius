let User = require("../models/User");
let AuthenticateToken = require("../jwt/AuthenticateToken");
const GenerateAccessToken = require("../jwt/GenerateAccessToken");
const dgram = require("dgram");
const radius = require("radius");

class UserController {
  read(req, res) {
    User.read(req.params.id).then(u => res.json(u));
  }
  all(req, res) {
    User.readAll(req).then(u => {
      res.json(u);
    });
  }
  register(req, res) {
    let payload = {
      email: req.body.email,
      role: req.body.role,
      password: req.body.password
    };
    User.create(payload).then(i => {
      res.json({ token: i });
    });
  }
  login(req, res) {
    let payload = {
      email: req.body.email,
      password: req.body.password
    };
    if (!req.body.sso) {
      User.authorize(payload).then(i => {
        res.json({ token: i });
      });
    } else {
      console.log("Access-Request for " + "bob");
      var packet = radius.encode({
        code: "Access-Request",
        secret: "testing123",
        attributes: [
          ["NAS-IP-Address", "127.0.0.1"],
          ["User-Name", req.body.email],
          ["User-Password", req.body.password]
        ]
      });
      //const message = Buffer.from("Some bytes");
      const client = dgram.createSocket("udp4");
      client.connect(1812, "localhost", err => {
        client.on("message", msg => {
          let decoded = radius.decode({ packet: msg, secret: "testing123" });
          console.log(decoded);

          client.close();
          if (decoded.code === "Access-Accept") {
            res.json({ token: GenerateAccessToken.jwt(req.body.email) });
          } else {
            res.json({ message: "no SSO user found" });
          }
        });
        client.send(packet, err => {
          if (err) {
            console.log(err);
            client.close();
            res.json({ message: "no SSO user found" });
          }
        });
      });
    }
  }
  delete(req, res) {
    if (typeof req.query.id === "string") {
      User.delete(req.query.id);
    } else if (typeof req.query.id === "object") {
      req.query.id.forEach(id => {
        User.delete(id);
      });
    }
  }
  update(req, res) {
    // req.params.id is user to edit
    // req.query has things to edit
    if (req.params.id) {
      User.update(req.params.id, req.query);
    } else {
      res.json({ error: "no id provided to edit" });
    }
  }
  authenticateToken(req, res) {
    const tokenStatus = AuthenticateToken.auth(req.body.token);
    if (tokenStatus) {
      const newToken = GenerateAccessToken.jwt(tokenStatus.data);
      res.json({ status: newToken });
    } else {
      res.json({ status: false });
    }
  }
  validateToken(req, res) {
    const tokenStatus = AuthenticateToken.auth(req.body.token);
    if (tokenStatus) {
      res.json({ status: true });
    } else {
      res.json({ status: false });
    }
  }
}

module.exports = new UserController();
